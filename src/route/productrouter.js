import { Router } from "express";
import { Product } from "../schema/model.js";

export let productRouter = Router();
productRouter
  .route("/")
  .post(async (req, res, next) => {
    let data = req.body;
    try {
      let result = await Product.create(data);
      console.log(result);
      res.status(201).json({
        success: true,
        message: "product created successfully.",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
    //store data in product databse
  })

  .get(async (req, res, next) => {
    //it must give all products from db to postman

    try {
      let result = await Product.find({});
      res.status(200).json({
        success: true,
        message: "product read successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({ success: false, message: error.message });
    }
  });

productRouter
  .route("/:id")
  .get(async (req, res, next) => {
    try {
      let result = await Product.findById(req.params.id);
      console.log(result);
      res.status(200).json({
        success: true,
        message: "product read successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res, next) => {
    try {
      let result = await Product.findByIdAndDelete(req.params.id);
      res.status(200).json({
        success: true,
        message: "product delete successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({ success: false, message: error.message });
    }
  })
  .patch(async (req, res, next) => {
    try {
      let result = await Product.findByIdAndUpdate(req.params.id, req.body);
      res.status(201).json({
        success: true,
        message: "product is updated!!",
        result: result,
      });
    } catch (error) {
      res.status(400).json({ success: false, message: error.message });
    }
  });

/* 
Product.create(req.body)=>create
Product.find({})=>read all
Product.findById(123)=> read specific
Product.findByAndUpdate(id,req.body)
Product.findByAndDelete
in js some code runs at last
to check if it runs at last do console
if it give promises then it will run at last

to run the promise code in a specific line
use await and async concept
 
 */

/* 

success
post, patch => 201
for all => 200


failure
status code 400

*/
