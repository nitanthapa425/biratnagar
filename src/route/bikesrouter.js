import { Router } from "express";

export let bikesRouter = Router();
bikesRouter
  .route("/")
  .post((req, res, next) => {
    res.json({
      success: true,
      message: "bikes created successfully.",
    });
  })
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "bikes read successfully.",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "bikes updated successfully.",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "bikes deleted successfully.",
    });
  });
bikesRouter.route("/:id").post((req, res, next) => {
  res.json({
    success: true,
    message: "bikes created successfully.",
  });
});
bikesRouter
  .route("/:id1")
  .post((req, res, next) => {
    res.json({
      success: true,
      message: "bikes created successfully.",
    });
  })
  .get((req, res, next) => {
    console.log(req.body);
    console.log(req.params); 
    res.json({
      success: true,
      message: "bikes read successfully.",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "bikes updated successfully.",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "bikes deleted successfully.",
    });
  });
