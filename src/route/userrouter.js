import { Router } from "express";
import { User } from "../schema/model.js";

export let userRouter = Router();
userRouter
  .route("/")
  .post(async (req, res, next) => {
    let data = req.body;
    try {
      let result = await User.create(data);
      console.log(result);
      res.status(201).json({
        success: true,
        message: "User is created successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res, next) => {
    try {
      let result = await User.find({});
      res.status(200).json({
        success: true,
        message: "User is read successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  });
userRouter
  .route("/:id")
  .get(async (req, res, next) => {
    try {
      let result = await User.findById(req.params.id);
      console.log(result);
      res.status(200).json({
        success: true,
        message: "User read successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res, next) => {
    try {
      let result = await User.findByIdAndDelete(req.params.id);
      res.status(200).json({
        success: true,
        message: "product delete successfully",
        result: result,
      });
    } catch (error) {
      res.status(400).json({ success: false, message: error.message });
    }
  })
  .patch(async (req, res, next) => {
    try {
      let result = await User.findByIdAndUpdate(req.params.id, req.body);
      res.status(201).json({
        success: true,
        message: "User is updated!!",
        result: result,
      });
    } catch (error) {
      req.status(400).json({ success: false, message: error.message });
    }
  });
