/* define array=> model
name,object*/

import { model } from "mongoose";
import { productSchema } from "./productschema.js";
import { userSchema } from "./userschema.js";

export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
/* variable name must be same as model name 
model name must be fisrtletter capital and singular ie. product not products*/
