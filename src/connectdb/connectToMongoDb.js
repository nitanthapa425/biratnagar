/* eslint-disable no-console */
import mongoose from "mongoose";

const connectToMongoDb = () => {
  try {
    mongoose.connect("mongodb://0.0.0.0:27017/mydb");
    console.log("application is connected to mongodb successfully.");
  } catch (error) {
    console.log(error.message);
  }
};

export default connectToMongoDb;

/* 
status code

success (2XX)
c => 201
r =>200
u => 201
d =>200


failure
400



*/
